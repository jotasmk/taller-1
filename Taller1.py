# Ejercicio 1
#La función Hazard regresa una determinada suma desde 1 hasta un número
#entero n
def Hazard(n):
    suma=0
    for i in range(1,n+1):
        suma = suma + ((-1)**(i+1))*2/(2*i-1)
    return suma


print("Ejercicio 1, algunos ejemplos:")
print("Hazard(5)=",Hazard(5))
print("Hazard(-2)=",Hazard(-2))
print("Hazard(23)=",Hazard(23))




# Ejercicio 2
#Dado un número n, la función esPrimo indica si este es primo o no
#Regresa un booliano
def esPrimo(n):
    suma = 0
    for i in range(1,abs(n)+1):
        if abs(n)%i == 0:
            suma = suma + 1
    return 2 == suma

#Dado un número n, la función es2N1 indica si este se puede escribir como
#una potencia de 2, menos 1.
#Devuelve un booliano
def es2N1(x):
    n = 0
    while ((2**n) - 1) < x:
        n = n + 1
    return x == ((2**n) - 1)

#Dado un número n, Lukaku devuelve un resultado res que verifica que desde
#1 hasta res hay n números primos que son 2N1

def Lukaku(n):
    lista=[]
    i=1
    count=0
    while count != n:
        if esPrimo(i) == True and es2N1(i) == True:
            count = count + 1
        i = i + 1
    return i - 1

print("Ejercicio 2, algunos ejemplos:")
print("Lukaku(1)=",Lukaku(1))
print("Lukaku(2)=",Lukaku(2))
print("Lukaku(4)=",Lukaku(4))

